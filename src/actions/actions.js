export const ADD_TODO = "ADD_TODO";
export const DELETE_TODO = "DELETE_TODO";
export const CLEAR_COMPLETED_TODOS = "CLEAR_COMPLETED_TODOS";
export const TOGGLE_TODO = "TOGGLE_TODO";

export function addTodo(title) {
  let todoItem = {};
  todoItem["userid"] = 1;
  todoItem["id"] = Math.floor(Math.random() * 10000);
  todoItem["title"] = title;
  todoItem["completed"] = false;
  return { type: ADD_TODO, payload: todoItem };
}
export function deleteTodo(toDeleteId) {
  return { type: DELETE_TODO, toDeleteId };
}
export function clearCompletedTodos() {
  console.log("sent to reducer");
  return { type: CLEAR_COMPLETED_TODOS };
}
export function toggleTodo(toToggleId) {
  return { type: TOGGLE_TODO, payload: toToggleId };
}
