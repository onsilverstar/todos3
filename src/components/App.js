import React, { Component } from "react";
import { Route } from "react-router-dom";
import { NavLink } from "react-router-dom";
import "../index.css";
import { addTodo } from "../actions/actions.js";
import {
  clearCompletedTodos,
  toggleTodo,
  deleteTodo
} from "../actions/actions.js";
import TodoList from "../components/TodoList";
import { connect } from "react-redux";

export class App extends Component {
  constructor(props) {
    super();
  }
  componentDidMount() {
    //const { todos } = this.props.todos;
  }
  handleAddTodo = event => {
    if (event.key === "Enter") {
      this.props.addTodo(event.target.value);
      console.log(this.props.todos);
      event.target.value = "";
    }
  };

  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input
            className="new-todo"
            placeholder="What needs to be done?"
            autoFocus
            onKeyDown={this.handleAddTodo}
          />
        </header>
        <Route
          exact
          path="/"
          render={() => <TodoList todos={this.props.todos} />}
        />
        <Route
          exact
          path="/active"
          render={() => (
            <TodoList
              todos={this.props.todos.filter(
                element => element.completed === false
              )}
            />
          )}
        />
        <Route
          exact
          path="/completed"
          render={() => (
            <TodoList
              todos={this.props.todos.filter(
                element => element.completed === true
              )}
            />
          )}
        />

        <footer className="footer">
          <span className="todo-count">
            <strong>
              {this.props.todos.filter(el => el.completed !== true).length}
            </strong>{" "}
            item(s) left
          </span>
          <ul className="filters">
            <li>
              <NavLink exact to="/" activeClassName="selected">
                All
              </NavLink>
            </li>
            <li>
              <NavLink exact to="/active" activeClassName="selected">
                Active
              </NavLink>
            </li>
            <li>
              <NavLink exact to="/completed" activeClassName="selected">
                Completed
              </NavLink>
            </li>
          </ul>
          <button
            className="clear-completed"
            onClick={event => {
              this.props.clearCompletedTodos(event);
              console.log("action dispatched");
            }}
          >
            Clear completed
          </button>
        </footer>
      </section>
    );
  }
}

const mapDispatchToProps = {
  addTodo,
  clearCompletedTodos,
  toggleTodo,
  deleteTodo
};
function mapStateToProps(state) {
  return { todos: state.todos };
}
export default connect(mapStateToProps, mapDispatchToProps)(App);
