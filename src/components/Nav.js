import React from "react";
import { Link } from "react-router-dom";

Navigation(props);
{
  return (
    <ul>
      <li>
        <Link to="/">All</Link>
      </li>
      <li>
        <Link to="/Active">Active</Link>
      </li>
      <li>
        <Link to="/Completed">Completed</Link>
      </li>
    </ul>
  );
}
