import React, { Component } from "react";
import "../index.css";
export class TodoItem extends Component {
  constructor(props) {
    super();
  }
  render() {
    return (
      <li className={this.props.completed ? "completed" : ""}>
        <div className="view">
          <input
            className="toggle"
            type="checkbox"
            checked={this.props.completed}
            onChange={this.props.handleToggle}
          />
          <label>{this.props.title}</label>
          <button className="destroy" onClick={this.props.handleToDelete} />
        </div>
      </li>
    );
  }
}
export default TodoItem;
