import React, { Component } from "react";
import "../index.css";
import TodoItem from "../components/TodoItem";
import { connect } from "react-redux";
import { deleteTodo, toggleTodo } from "../actions/actions.js";

export class TodoList extends Component {
  constructor(props) {
    super();
  }

  render() {
    return (
      <section className="main">
        <ul className="todo-list">
          {this.props.todos.map(todo => (
            <TodoItem
              key={todo.id}
              title={todo.title}
              completed={todo.completed}
              id={todo.id}
              handleToDelete={event => this.props.deleteTodo(todo.id)}
              handleToggle={event => this.props.toggleTodo(todo.id)}
            />
          ))}
        </ul>
      </section>
    );
  }
}

const mapDispatchToProps = {
  toggleTodo,
  deleteTodo
};
export default connect(null, mapDispatchToProps)(TodoList);
