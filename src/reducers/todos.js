import todosList from ".././todos.json";
//import { CLEAR_COMPLETED_TODOS } from "../actions/actions.js/index.js";
export function todoReducer(state = { todos: todosList }, action) {
  switch (action.type) {
    case "ADD_TODO": {
      let todoListCopy = { ...state };

      todoListCopy.todos.push(action.payload);
      console.log({ todos: todoListCopy.todos });
      return { todos: todoListCopy.todos };
    }
    case "DELETE_TODO": {
      const myCopy = state.todos.slice();
      const modifiedCopy = myCopy.filter(
        element => element.id !== action.toDeleteId
      );
      return { todos: modifiedCopy };
    }
    case "TOGGLE_TODO": {
      const todoListCopy = state.todos.slice();
      const newtodoListCopy = todoListCopy.map(elementObject => {
        if (elementObject.id === action.payload) {
          elementObject.completed = !elementObject.completed;
          return elementObject;
        }
        return elementObject;
      });
      console.log("fired!");
      return { todos: newtodoListCopy };
    }

    case "CLEAR_COMPLETED_TODOS": {
      console.log("reducer received action");
      const myCopy = state.todos.slice();
      const Copy = myCopy.filter(element => element.completed === false);
      return { todos: Copy };
    }
    default:
      return state;
  }
}
export default todoReducer;
